package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Message;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Message entity.
 */

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
	
	@Query("Select m from Message m where m.receiver.id=?1 AND m.sender.id=?2"
			+ "OR "
			+ "m.receiver.id=?2 AND m.sender.id=?1")
	List<Message> findAllMessageBetweenUsers(Long sender, Long receiver);
	
	List<Message> findAllByDateBefore(Date date);
	
}
