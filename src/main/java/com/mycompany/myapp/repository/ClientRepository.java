package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Client;
import com.mycompany.myapp.domain.User;

import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Client entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
	
	Optional<Client> findOneByEmailIgnoreCase(String email);
}
