package com.mycompany.myapp.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Client.
 */
@Entity
@Table(name = "client")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "email")
    private String email;

    @Column(name = "tel")
    private String tel;

    @Column(name = "nom_ent")
    private String nomEnt;

    @Column(name = "adresse_ent")
    private String adresseEnt;

    @Column(name = "adresse_fact")
    private String adresseFact;

    @Column(name = "statut")
    private String statut;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "receiver")
    private Set<Message> messagesRecu = new HashSet<>();

    @OneToMany(mappedBy = "sender")
    private Set<Message> messagesEnvoyer = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Client nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Client prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public Client email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public Client tel(String tel) {
        this.tel = tel;
        return this;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getNomEnt() {
        return nomEnt;
    }

    public Client nomEnt(String nomEnt) {
        this.nomEnt = nomEnt;
        return this;
    }

    public void setNomEnt(String nomEnt) {
        this.nomEnt = nomEnt;
    }

    public String getAdresseEnt() {
        return adresseEnt;
    }

    public Client adresseEnt(String adresseEnt) {
        this.adresseEnt = adresseEnt;
        return this;
    }

    public void setAdresseEnt(String adresseEnt) {
        this.adresseEnt = adresseEnt;
    }

    public String getAdresseFact() {
        return adresseFact;
    }

    public Client adresseFact(String adresseFact) {
        this.adresseFact = adresseFact;
        return this;
    }

    public void setAdresseFact(String adresseFact) {
        this.adresseFact = adresseFact;
    }

    public String getStatut() {
        return statut;
    }

    public Client statut(String statut) {
        this.statut = statut;
        return this;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public User getUser() {
        return user;
    }

    public Client user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Message> getMessagesRecu() {
        return messagesRecu;
    }

    public Client messagesRecu(Set<Message> messages) {
        this.messagesRecu = messages;
        return this;
    }

    public Client addMessageRecu(Message message) {
        this.messagesRecu.add(message);
        message.setReceiver(this);
        return this;
    }

    public Client removeMessageRecu(Message message) {
        this.messagesRecu.remove(message);
        message.setReceiver(null);
        return this;
    }

    public void setMessagesRecu(Set<Message> messages) {
        this.messagesRecu = messages;
    }

    public Set<Message> getMessagesEnvoyer() {
        return messagesEnvoyer;
    }

    public Client messagesEnvoyer(Set<Message> messages) {
        this.messagesEnvoyer = messages;
        return this;
    }

    public Client addMessageEnvoyer(Message message) {
        this.messagesEnvoyer.add(message);
        message.setSender(this);
        return this;
    }

    public Client removeMessageEnvoyer(Message message) {
        this.messagesEnvoyer.remove(message);
        message.setSender(null);
        return this;
    }

    public void setMessagesEnvoyer(Set<Message> messages) {
        this.messagesEnvoyer = messages;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Client)) {
            return false;
        }
        return id != null && id.equals(((Client) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Client{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", email='" + getEmail() + "'" +
            ", tel='" + getTel() + "'" +
            ", nomEnt='" + getNomEnt() + "'" +
            ", adresseEnt='" + getAdresseEnt() + "'" +
            ", adresseFact='" + getAdresseFact() + "'" +
            ", statut='" + getStatut() + "'" +
            "}";
    }
}
