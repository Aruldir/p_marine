package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

/**
 * A Message.
 */
@Entity
@Table(name = "message")
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "contenu")
    private String contenu;
    
    @Column(name = "date")
    private Date date;

    @ManyToOne
    @JsonIgnoreProperties(value = "messages", allowSetters = true)
    private Client receiver;

    @ManyToOne
    @JsonIgnoreProperties(value = "messages", allowSetters = true)
    private Client sender;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public Message contenu(String contenu) {
        this.contenu = contenu;
        return this;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Client getReceiver() {
        return receiver;
    }

    public Message receiver(Client client) {
        this.receiver = client;
        return this;
    }

    public void setReceiver(Client client) {
        this.receiver = client;
    }

    public Client getSender() {
        return sender;
    }

    public Message sender(Client client) {
        this.sender = client;
        return this;
    }

    public void setSender(Client client) {
        this.sender = client;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Message)) {
            return false;
        }
        return id != null && id.equals(((Message) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Message{" +
            "id=" + getId() +
            ", contenu='" + getContenu() + "'" +
            ", date='" + getDate() + "'" +
            "}";
    }

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
