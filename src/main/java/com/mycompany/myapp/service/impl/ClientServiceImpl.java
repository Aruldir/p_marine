package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.ClientService;
import com.mycompany.myapp.service.MessageService;
import com.mycompany.myapp.domain.Client;
import com.mycompany.myapp.domain.Message;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.ClientRepository;
import com.mycompany.myapp.repository.UserRepository;
import com.mycompany.myapp.service.dto.ClientDTO;
import com.mycompany.myapp.service.mapper.ClientMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Client}.
 */
@Service
@Transactional
public class ClientServiceImpl implements ClientService {

    private final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    private final ClientRepository clientRepository;

    private final ClientMapper clientMapper;
    
    private final UserRepository userRepository;
    
    private final MessageService messageService;

    public ClientServiceImpl(ClientRepository clientRepository, ClientMapper clientMapper, UserRepository userRepository,MessageService messageService) {
        this.clientRepository = clientRepository;
        this.clientMapper = clientMapper;
        this.userRepository = userRepository;
        this.messageService = messageService;
    }

    @Override
    public ClientDTO save(ClientDTO clientDTO) {
        log.debug("Request to save Client : {}", clientDTO);
        Client client = clientMapper.toEntity(clientDTO);
        client = clientRepository.save(client);
        return clientMapper.toDto(client);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ClientDTO> findAll() {
        log.debug("Request to get all Clients");
        return clientRepository.findAll().stream()
            .map(clientMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ClientDTO> findOne(Long id) {
        log.debug("Request to get Client : {}", id);
        return clientRepository.findById(id)
            .map(clientMapper::toDto);
    }

    @Override
    public void delete(Long id) {
    	Client c= new Client();
    	User u= new User();
    	ArrayList<Message> messages= new ArrayList<Message>();
    	c= clientRepository.findById(id).orElseThrow();
    	u= c.getUser();
    	messages.addAll(c.getMessagesEnvoyer());
    	messages.addAll(c.getMessagesRecu());
    	for(Message m : messages) {
    		messageService.delete(m.getId());
    	}
    	messages = null;
        log.debug("Request to delete Client : {}", id);
        clientRepository.deleteById(id);
        log.debug("Request to delete User : {}", u.getLogin());
        userRepository.delete(u);
    }

	@Override
	@Transactional(readOnly = true)
	public Optional<ClientDTO> findOnebyEmail(String email) {
		return clientRepository.findOneByEmailIgnoreCase(email).map(clientMapper::toDto);
	}

	
}
