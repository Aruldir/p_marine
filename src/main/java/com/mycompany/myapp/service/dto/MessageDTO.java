package com.mycompany.myapp.service.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.Message} entity.
 */
public class MessageDTO implements Serializable {
    
    private Long id;

    private String contenu;

    private Date date;	

    private Long receiverId;

    private Long senderId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long clientId) {
        this.receiverId = clientId;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long clientId) {
        this.senderId = clientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MessageDTO)) {
            return false;
        }

        return id != null && id.equals(((MessageDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MessageDTO{" +
            "id=" + getId() +
            ", contenu='" + getContenu() + "'" +
            ", date='" + getDate() + "'" +
            ", receiverId=" + getReceiverId() +
            ", senderId=" + getSenderId() +
            "}";
    }

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
