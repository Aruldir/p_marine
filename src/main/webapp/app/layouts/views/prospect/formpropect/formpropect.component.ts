
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'app/entities/message/message.service';


@Component({
  selector: 'jhi-formpropect',
  templateUrl: './formpropect.component.html',
  styleUrls: ['./formpropect.component.scss']
})
export class FormpropectComponent implements OnInit {
  editForm = this.fb.group({
    nom: '',
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    typepresta: '',
    telephone: '',
    desc: '',
  });
  constructor(private fb: FormBuilder, private mservice: MessageService) { }

  ngOnInit(): void {

  }

  private createMessage(): string{
    return 'Bonjour,\r Voici un potentiel client : \r- Son nom ou Nom d\'entreprise : '+this.editForm.get(['nom'])!.value+'\r- son email :'+this.editForm.get(['email'])!.value+'\r- son tel :'+this.editForm.get(['telephone'])!.value+'\r -le Type de Prestation:' + this.editForm.get(['typepresta'])?.value  + '\rLa Description qu\'il a laissé :'+this.editForm.get(['desc'])!.value+'\r \r Cordialement, \r Ton esclave de Robot \r Ps: Pense à une ptit augmentation.'
  }
  save(): void {
    const message = this.createMessage()
    this.mservice.prospect(message).subscribe(
      () => (window.history.back())

    );

  }








}
