import { Component, OnInit} from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client/client.service';
import { ClientDeleteDialogComponent } from 'app/entities/client/client-delete-dialog.component';


@Component({
  selector: 'jhi-lesclients',
  templateUrl: './lesclients.component.html',
  styleUrls: ['./lesclients.component.scss']
})
export class LesclientsComponent implements OnInit {
  clients?: IClient[];
  eventSubscriber?: Subscription;

  constructor(protected clientService: ClientService,protected eventManager: JhiEventManager, protected modalService: NgbModal) { }

  loadAll(): void {
    this.clientService.query().subscribe(
      (res: HttpResponse<IClient[]>) => ((this.clients = res.body || []), this.suppMarine())
      );
  }

  ngOnInit(): void {
    this.loadAll();

    this.registerChangeInClients();
  }
  trackId(index: number, item: IClient): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInClients(): void {
    this.eventSubscriber = this.eventManager.subscribe('clientListModification', () => this.loadAll());
  }

  suppMarine(): void {
    this.clients?.splice((this.clients.findIndex(c=> c.id===9999)),1);
  }

  delete(client: IClient): void {
    const modalRef = this.modalService.open(ClientDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.client = client;
  }
}
