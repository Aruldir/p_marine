import { HttpResponse } from '@angular/common/http';
import {AfterViewInit, Component, OnInit} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MessageService } from 'app/entities/message/message.service';
import { IMessage, Message } from 'app/shared/model/message.model';
import { JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'jhi-messagerie-marine',
  templateUrl: './messagerie-marine.component.html',
  styleUrls: ['./messagerie-marine.component.scss']
})
export class MessagerieMarineComponent implements OnInit, AfterViewInit {
  editForm = this.fb.group({
    message : ''
  });
  idclient? :number;
  error?: boolean;
  Messages : IMessage[]= [];
  eventSubscriber?: Subscription;
  constructor(
    private messageService: MessageService,
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) { }

  ngAfterViewInit(): void {
    this.scrolltobottom('scrollul');
    // this.Messages.sort((a,b)=>new Date(this.getT(a.date)).getTime() - new Date(this.getT(b.date)).getTime());

  }

  ngOnInit(): void {
    this.searchmessage();
    this.viewallmessage();
    this.scrolltobottom('scrollul');
  }



  private getT(date?: Date): number {
    return date != null ? date.getTime() : 0;
  }

  searchmessage(): void{
    this.activatedRoute.paramMap.subscribe(params => {
      this.idclient = Number(params.get('id'));
    });
  }


  trackId(index: number, item: IMessage): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  save(): void {
    const msg=this.createmsg();
    this.messageService.create(msg).subscribe(
      ()=>(window.location.reload())
    );

  }

  createmsg(): IMessage {
    return {
      ...new Message(),
      contenu : this.editForm.get(['message'])!.value,
      date : new Date(),
      senderId: 9999,
      receiverId:this.idclient ,
    }
  }

  viewallmessage(): void {
    if(this.idclient !==  undefined){
      this.messageService.findBetween2(9999,this.idclient).subscribe((res: HttpResponse<IMessage[]>) => (this.Messages = res.body || []));

    }else {

      this.error = true;
    }

  }

  scrolltobottom(arg0: string): void {
    const obj = document.getElementById(arg0);
    if(obj){
      obj.scrollIntoView(false);
      // eslint-disable-next-line no-console
      console.log(obj.scrollHeight +' '+ obj.scrollTop)
    }
  }

  registerChangeInMessages(): void {
    this.eventSubscriber = this.eventManager.subscribe('messageListModification', () => this.viewallmessage());
  }
}

