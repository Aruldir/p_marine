
import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { ClientService } from 'app/entities/client/client.service';
import { MessageService } from 'app/entities/message/message.service';
import { IMessage, Message } from 'app/shared/model/message.model';
import { JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs';

@Component({
  selector: 'jhi-messagerie',
  templateUrl: './messagerie.component.html',
  styleUrls: ['./messagerie.component.scss']
})
export class MessagerieComponent implements OnInit {
  editForm = this.fb.group({
    message : ''
  });
  idclient? :number;
  error?: boolean;
  ids?: number[];
  Messages : IMessage[]= [];
  eventSubscriber?: Subscription;
  constructor(
    private accountService : AccountService,
    private clientService: ClientService,
    private messageService: MessageService,
    protected eventManager: JhiEventManager,
    private fb: FormBuilder,
    private mservice: MessageService,
  ) { }

  ngOnInit(): void {

    this.trouverMessage();
    // this.Messages.sort((a,b)=>new Date(this.getT(a.date)).getTime() - new Date(this.getT(b.date)).getTime());
    this.scrolltobottom('scrollul');
  }
  private getT(date?: Date): number {
    return date != null ? date.getTime() : 0;
  }

  private infoCompte(): Account {
    let account :Account=new Account(false,[],'','','','','','');
    this.accountService.getAuthenticationState().subscribe(
      (res)=>{
        if(res!== null){
          account=res;
        }
      }
    )
    return account;
  }

   trouverMessage():void{
    this.clientService.findbyemail(this.infoCompte().email).subscribe(
      (data)=> {
        this.idclient= data.body?.id;
        this.viewallmessage();
      },

    );
  }

  trackId(index: number, item: IMessage): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

   createMessage(): string{
    return 'Bonjour,\rTon client numéro '+ this.idclient+ ' ta envoyé un message.\r\rCordialement,\rTon robot préféré !!!!!'
  }

   envoieMail(): void {
    const message = this.createMessage();
    this.mservice.prospect(message).subscribe(
      ()=>{
        // eslint-disable-next-line no-console
        console.log('test');
      }
    );
  }
  save(): void {
    this.envoieMail();
    const msg=this.createmsg();
    this.messageService.create(msg).subscribe(
      ()=>(
        window.location.reload()
        )
    );

  }

  createmsg(): IMessage {
    return {
      ...new Message(),
      contenu : this.editForm.get(['message'])!.value,
      date : new Date(),
      senderId: this.idclient,
      receiverId: 9999,

    }
  }

  viewallmessage(): void {
    if(this.idclient !==  undefined){
      this.messageService.findBetween2(this.idclient,9999).subscribe((res: HttpResponse<IMessage[]>) => (this.Messages = res.body || []));

    }else {

      this.error = true;
    }

  }


  registerChangeInMessages(): void {
    this.eventSubscriber = this.eventManager.subscribe('messageListModification', () => this.viewallmessage());
  }

  scrolltobottom(arg0: string): void {
    const obj = document.getElementById(arg0);
    if(obj){
      obj.scrollTop = obj.scrollHeight;
    }
  }
}
