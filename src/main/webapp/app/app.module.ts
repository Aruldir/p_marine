import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { ProjetMarineSharedModule } from 'app/shared/shared.module';
import { ProjetMarineCoreModule } from 'app/core/core.module';
import { ProjetMarineAppRoutingModule } from './app-routing.module';
import { ProjetMarineHomeModule } from './home/home.module';
import { ProjetMarineEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { MessagerieComponent } from './layouts/views/client/messagerie/messagerie.component';
import { FichierComponent } from './layouts/views/client/fichier/fichier.component';
import { LesclientsComponent } from './layouts/views/marine/lesclients/lesclients.component';
import { FormpropectComponent } from './layouts/views/prospect/formpropect/formpropect.component';
import { MessagerieMarineComponent } from './layouts/views/marine/messagerie-marine/messagerie-marine.component';
import { CGUComponent } from './layouts/views/cgu/cgu.component';




@NgModule({
  imports: [
    BrowserModule,
    ProjetMarineSharedModule,
    ProjetMarineCoreModule,
    ProjetMarineHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    ProjetMarineEntityModule,
    ProjetMarineAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent, MessagerieComponent, FichierComponent, LesclientsComponent,  FormpropectComponent, MessagerieMarineComponent, CGUComponent],
  bootstrap: [MainComponent],
})
export class ProjetMarineAppModule {}
