import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router} from '@angular/router';
import { Observable } from 'rxjs';

import { IClient, Client } from 'app/shared/model/client.model';
import { ClientService } from './client.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-client-update',
  templateUrl: './client-update.component.html',
})
export class ClientUpdateComponent implements OnInit {
  isSaving = false;
  user!: IUser;
  login!: any;
  editForm = this.fb.group({
    id: [],
    nom: [],
    email: [],
    prenom: [],
    tel: [],
    nomEnt: [],
    adresseEnt: [],
    adresseFact: [],
    statut: [],
    userId: [],
  });

  constructor(
    protected clientService: ClientService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ client }) => {
      this.updateForm(client);
    });

  }

  updateForm(client: IClient): void {
    this.editForm.patchValue({
      id: client.id,
      nom: client.nom,
      prenom: client.prenom,
      email: client.email,
      tel: client.tel,
      nomEnt: client.nomEnt,
      adresseEnt: client.adresseEnt,
      adresseFact: client.adresseFact,
      statut: client.statut,
      userId: client.userId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;

    const client = this.createFromForm();
    this.subscribeToSaveResponse(this.clientService.update(client));

  }

  private createFromForm(): IClient {
    return {
      ...new Client(),
      id:this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      email:this.editForm.get(['email'])!.value,
      tel: this.editForm.get(['tel'])!.value,
      nomEnt: this.editForm.get(['nomEnt'])!.value,
      adresseEnt: this.editForm.get(['adresseEnt'])!.value,
      adresseFact: this.editForm.get(['adresseFact'])!.value,
      statut: this.editForm.get(['statut'])!.value,
      userId: this.editForm.get(['userId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IClient>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.router.navigate(['/marine/mesclients']);
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
