import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router} from '@angular/router';
import { Observable } from 'rxjs';

import { IClient, Client } from 'app/shared/model/client.model';
import { ClientService } from './client.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-client-create',
  templateUrl: './client-create.component.html',
})
export class ClientCreateComponent implements OnInit {
  isSaving = false;
  user!: IUser;
  login!: any;
  editForm = this.fb.group({
    id: [],
    nom: [],
    prenom: [],
    tel: [],
    nomEnt: [],
    adresseEnt: [],
    adresseFact: [],
    statut: [],
  });

  constructor(
    protected clientService: ClientService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.searchuser();
  }
  searchuser(): void{
    this.activatedRoute.paramMap.subscribe(params => {
      this.login = params.get('login');

    });
    this.userService.find(this.login).subscribe(user=> this.user=user);

  }

  updateForm(client: IClient): void {
    this.editForm.patchValue({
      id: client.id,
      nom: client.nom,
      prenom: client.prenom,
      tel: client.tel,
      nomEnt: client.nomEnt,
      adresseEnt: client.adresseEnt,
      adresseFact: client.adresseFact,
      statut: client.statut,
      userId: client.userId,
    });
  }

  previousState(): void {
    if(this.user.login){
      this.userService.delete(this.user.login).subscribe(
        ()=>{this.router.navigate(['/marine/mesclients'])},
      );

    }


  }

  save(): void {
    this.isSaving = true;
    const client = this.createFromForm();
    if (client.id !== undefined) {
      this.subscribeToSaveResponse(this.clientService.update(client));
    } else {
      this.subscribeToSaveResponse(this.clientService.create(client));
    }
  }

  private createFromForm(): IClient {
    return {
      ...new Client(),
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      email: this.user.email,
      tel: this.editForm.get(['tel'])!.value,
      nomEnt: this.editForm.get(['nomEnt'])!.value,
      adresseEnt: this.editForm.get(['adresseEnt'])!.value,
      adresseFact: this.editForm.get(['adresseFact'])!.value,
      statut: this.editForm.get(['statut'])!.value,
      userId: this.user.id,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IClient>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.router.navigate(['/marine/mesclients']);
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
