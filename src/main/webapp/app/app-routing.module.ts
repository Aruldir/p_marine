import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute } from './layouts/error/error.route';
import { navbarRoute } from './layouts/navbar/navbar.route';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';
import { Authority } from 'app/shared/constants/authority.constants';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { MessagerieComponent } from './layouts/views/client/messagerie/messagerie.component';
// import { FichierComponent } from './layouts/views/client/fichier/fichier.component';
import { LesclientsComponent } from './layouts/views/marine/lesclients/lesclients.component';
import { FormpropectComponent } from './layouts/views/prospect/formpropect/formpropect.component';
import { MessagerieMarineComponent } from './layouts/views/marine/messagerie-marine/messagerie-marine.component';
import { CGUComponent } from './layouts/views/cgu/cgu.component';



const LAYOUT_ROUTES = [navbarRoute, ...errorRoute];

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'admin',
          data: {
            authorities: [Authority.ADMIN],
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./admin/admin-routing.module').then(m => m.AdminRoutingModule),
        },
        {
          path: 'prendrecontact',
          component: FormpropectComponent,
        },
        {
          path: 'CGU',
          component: CGUComponent,
        },
        {
          path: 'client/messagerie',
          component: MessagerieComponent,
          data: {
            authorities: [Authority.CLIENT],
          },
          canActivate: [UserRouteAccessService]
        },
        /* {
          path: 'client/fichier',
          component: FichierComponent,
          data: {
            authorities: [Authority.CLIENT],
          },
          canActivate: [UserRouteAccessService]
        },*/
        {
          path: 'marine/mesclients',
          component: LesclientsComponent,
          data: {
            authorities: [Authority.MARINE],
          },
          canActivate: [UserRouteAccessService]
        },
        {
          path: 'marine/message/:id',
          component: MessagerieMarineComponent,
          data: {
            authorities: [Authority.MARINE],
          },
          canActivate: [UserRouteAccessService]
        },
        {
          path: 'account',
          loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
        },
        ...LAYOUT_ROUTES,
      ],
      { enableTracing: DEBUG_INFO_ENABLED }
    ),
  ],
  exports: [RouterModule],
})
export class ProjetMarineAppRoutingModule {}
