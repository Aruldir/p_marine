export enum Authority {
  ADMIN = 'ROLE_ADMIN',
  USER = 'ROLE_USER',
  MARINE = 'ROLE_MARINE',
  CLIENT = 'ROLE_CLIENT'
}
