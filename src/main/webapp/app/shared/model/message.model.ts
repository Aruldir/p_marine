export interface IMessage {
  id?: number;
  contenu?: string;
  date?: Date;
  receiverId?: number;
  senderId?: number;
}

export class Message implements IMessage {
  constructor(public id?: number, public contenu?: string, public receiverId?: number, public senderId?: number, public date?: Date) {}
}
