import { IMessage } from 'app/shared/model/message.model';

export interface IClient {
  id?: number;
  nom?: string;
  prenom?: string;
  email?: string;
  tel?: string;
  nomEnt?: string;
  adresseEnt?: string;
  adresseFact?: string;
  statut?: string;
  userLogin?: string;
  userId?: number;
  messagesRecu?: IMessage[];
  messagesEnvoyer?: IMessage[];
}

export class Client implements IClient {
  constructor(
    public id?: number,
    public nom?: string,
    public prenom?: string,
    public email?: string,
    public tel?: string,
    public nomEnt?: string,
    public adresseEnt?: string,
    public adresseFact?: string,
    public statut?: string,
    public userLogin?: string,
    public userId?: number,
    public messagesRecu?: IMessage[],
    public messagesEnvoyer?: IMessage[]
  ) {}
}
