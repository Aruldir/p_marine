package com.mycompany.myapp.domain;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Message.class)
public abstract class Message_ {

	public static volatile SingularAttribute<Message, Date> date;
	public static volatile SingularAttribute<Message, Client> receiver;
	public static volatile SingularAttribute<Message, Client> sender;
	public static volatile SingularAttribute<Message, Long> id;
	public static volatile SingularAttribute<Message, String> contenu;

	public static final String DATE = "date";
	public static final String RECEIVER = "receiver";
	public static final String SENDER = "sender";
	public static final String ID = "id";
	public static final String CONTENU = "contenu";

}

