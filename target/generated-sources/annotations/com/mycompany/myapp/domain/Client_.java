package com.mycompany.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Client.class)
public abstract class Client_ {

	public static volatile SetAttribute<Client, Message> messagesRecu;
	public static volatile SingularAttribute<Client, String> adresseEnt;
	public static volatile SingularAttribute<Client, String> adresseFact;
	public static volatile SingularAttribute<Client, String> tel;
	public static volatile SingularAttribute<Client, Long> id;
	public static volatile SingularAttribute<Client, String> nomEnt;
	public static volatile SingularAttribute<Client, String> nom;
	public static volatile SingularAttribute<Client, String> prenom;
	public static volatile SingularAttribute<Client, User> user;
	public static volatile SingularAttribute<Client, String> email;
	public static volatile SingularAttribute<Client, String> statut;
	public static volatile SetAttribute<Client, Message> messagesEnvoyer;

	public static final String MESSAGES_RECU = "messagesRecu";
	public static final String ADRESSE_ENT = "adresseEnt";
	public static final String ADRESSE_FACT = "adresseFact";
	public static final String TEL = "tel";
	public static final String ID = "id";
	public static final String NOM_ENT = "nomEnt";
	public static final String NOM = "nom";
	public static final String PRENOM = "prenom";
	public static final String USER = "user";
	public static final String EMAIL = "email";
	public static final String STATUT = "statut";
	public static final String MESSAGES_ENVOYER = "messagesEnvoyer";

}

