package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Client;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.service.dto.ClientDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-02-02T19:02:19+0100",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.9.1 (AdoptOpenJDK)"
)
@Component
public class ClientMapperImpl implements ClientMapper {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<Client> toEntity(List<ClientDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Client> list = new ArrayList<Client>( dtoList.size() );
        for ( ClientDTO clientDTO : dtoList ) {
            list.add( toEntity( clientDTO ) );
        }

        return list;
    }

    @Override
    public List<ClientDTO> toDto(List<Client> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ClientDTO> list = new ArrayList<ClientDTO>( entityList.size() );
        for ( Client client : entityList ) {
            list.add( toDto( client ) );
        }

        return list;
    }

    @Override
    public ClientDTO toDto(Client client) {
        if ( client == null ) {
            return null;
        }

        ClientDTO clientDTO = new ClientDTO();

        clientDTO.setUserLogin( clientUserLogin( client ) );
        clientDTO.setUserId( clientUserId( client ) );
        clientDTO.setId( client.getId() );
        clientDTO.setNom( client.getNom() );
        clientDTO.setPrenom( client.getPrenom() );
        clientDTO.setEmail( client.getEmail() );
        clientDTO.setTel( client.getTel() );
        clientDTO.setNomEnt( client.getNomEnt() );
        clientDTO.setAdresseEnt( client.getAdresseEnt() );
        clientDTO.setAdresseFact( client.getAdresseFact() );
        clientDTO.setStatut( client.getStatut() );

        return clientDTO;
    }

    @Override
    public Client toEntity(ClientDTO clientDTO) {
        if ( clientDTO == null ) {
            return null;
        }

        Client client = new Client();

        client.setUser( userMapper.userFromId( clientDTO.getUserId() ) );
        client.setId( clientDTO.getId() );
        client.setNom( clientDTO.getNom() );
        client.setPrenom( clientDTO.getPrenom() );
        client.setEmail( clientDTO.getEmail() );
        client.setTel( clientDTO.getTel() );
        client.setNomEnt( clientDTO.getNomEnt() );
        client.setAdresseEnt( clientDTO.getAdresseEnt() );
        client.setAdresseFact( clientDTO.getAdresseFact() );
        client.setStatut( clientDTO.getStatut() );

        return client;
    }

    private String clientUserLogin(Client client) {
        if ( client == null ) {
            return null;
        }
        User user = client.getUser();
        if ( user == null ) {
            return null;
        }
        String login = user.getLogin();
        if ( login == null ) {
            return null;
        }
        return login;
    }

    private Long clientUserId(Client client) {
        if ( client == null ) {
            return null;
        }
        User user = client.getUser();
        if ( user == null ) {
            return null;
        }
        Long id = user.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
