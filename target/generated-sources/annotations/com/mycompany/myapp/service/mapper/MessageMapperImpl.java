package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Client;
import com.mycompany.myapp.domain.Message;
import com.mycompany.myapp.service.dto.MessageDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-02-02T19:02:19+0100",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.9.1 (AdoptOpenJDK)"
)
@Component
public class MessageMapperImpl implements MessageMapper {

    @Autowired
    private ClientMapper clientMapper;

    @Override
    public List<Message> toEntity(List<MessageDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Message> list = new ArrayList<Message>( dtoList.size() );
        for ( MessageDTO messageDTO : dtoList ) {
            list.add( toEntity( messageDTO ) );
        }

        return list;
    }

    @Override
    public List<MessageDTO> toDto(List<Message> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<MessageDTO> list = new ArrayList<MessageDTO>( entityList.size() );
        for ( Message message : entityList ) {
            list.add( toDto( message ) );
        }

        return list;
    }

    @Override
    public MessageDTO toDto(Message message) {
        if ( message == null ) {
            return null;
        }

        MessageDTO messageDTO = new MessageDTO();

        messageDTO.setSenderId( messageSenderId( message ) );
        messageDTO.setReceiverId( messageReceiverId( message ) );
        messageDTO.setId( message.getId() );
        messageDTO.setContenu( message.getContenu() );
        messageDTO.setDate( message.getDate() );

        return messageDTO;
    }

    @Override
    public Message toEntity(MessageDTO messageDTO) {
        if ( messageDTO == null ) {
            return null;
        }

        Message message = new Message();

        message.setReceiver( clientMapper.fromId( messageDTO.getReceiverId() ) );
        message.setSender( clientMapper.fromId( messageDTO.getSenderId() ) );
        message.setId( messageDTO.getId() );
        message.setContenu( messageDTO.getContenu() );
        message.setDate( messageDTO.getDate() );

        return message;
    }

    private Long messageSenderId(Message message) {
        if ( message == null ) {
            return null;
        }
        Client sender = message.getSender();
        if ( sender == null ) {
            return null;
        }
        Long id = sender.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long messageReceiverId(Message message) {
        if ( message == null ) {
            return null;
        }
        Client receiver = message.getReceiver();
        if ( receiver == null ) {
            return null;
        }
        Long id = receiver.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
